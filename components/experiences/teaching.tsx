import * as React from 'react';

const Teaching = () => (
  <div className="content">
    <h3>Teaching Experience</h3>
    <ul>
      <li>
        Teaching Assistant, KAIST <a href="http://cs101.kaist.ac.kr">CS101</a>{' '}
        (Introduction to Programming), 2018 Fall
      </li>
      <li>
        Teaching Assistant, KAIST CS374 (Introduction to HCI),{' '}
        <a href="https://kixlab.org/courses/cs374-spring-2017/index.html">
          2017 Spring
        </a>
        {', '}
        <a href="https://kixlab.org/courses/cs374-spring-2018/index.html">
          2018 Spring
        </a>
        <ul>
          <li>
            Led eight one-hour design studio of 12 undergraduate students
            throughout semester
          </li>
          <li>
            Designed three{' '}
            <a href="https://kixlab.org/courses/cs374-spring-2018/assignments.html">
              programming assignments
            </a>{' '}
            for HTML / JavaScript / Firebase
          </li>
          <li>
            Gave two tutorials on using{' '}
            <a href="https://kixlab.org/courses/cs374-spring-2018/assets/tutorials/tutorial3.pdf">
              JavaScript with Firebase
            </a>{' '}
            and{' '}
            <a href="https://kixlab.org/courses/cs374-spring-2017/assets/tutorials/Tutorial%204.pdf">
              Git
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
);

export default Teaching;
