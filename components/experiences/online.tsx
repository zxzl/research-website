import * as React from 'react';
const Online = () => (
  <div className="content">
    <h3>At Online</h3>
    <ul>
      <li>
        GitHub: <a href="http://github.com/zxzl">@zxzl</a>
      </li>
      <li>
        Twitter 🇰🇷: <a href="http://twitter.com/zxzl">@zxzl</a>
      </li>
      <li>
        Blog 🇰🇷 <a href="https://blog.shik.kr">blog.shik.kr</a>
      </li>
    </ul>
  </div>
);

export default Online;
