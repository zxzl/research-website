import * as React from 'react';

const Education = () => (
  <div className="content">
    <h3>Education</h3>
    <ul>
      <li>
        (Feb. 2019) M.S in{' '}
        <a href="http://cs.kaist.ac.kr">School of Computing</a>
        {', '}
        <a href="http://www.kaist.ac.kr">KAIST</a> (
        <a href="https://www.dropbox.com/s/6ne6ekwm0hiwe7h/thesis-ms-2019-hyeungshik.pdf?dl=0">
          thesis
        </a>
        )
      </li>
      <ul>
        <li>
          Focus on HCI, at <a href="http://kixlab.org">KIXLAB</a> (Advisor:{' '}
          <a href="http://juhokim.com">Juho Kim</a>)
        </li>
      </ul>
      <li>
        (Feb. 2017) B.S in{' '}
        <a href="http://cse.snu.ac.kr">Computer Science and Engineering</a>
        {', '}
        <a href="http://www.snu.ac.kr">Seoul National University</a>
      </li>
      <li>
        (Feb. 2017) B.A in{' '}
        <a href="http://isc.snu.ac.kr">Information Science and Culture</a>
        {', '}
        <a href="http://www.snu.ac.kr">Seoul National University</a>
        <ul>
          <li>
            Dual major from{' '}
            <a href="http://cls.snu.ac.kr">College of Liberal Studies</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
);

export default Education;
