import * as React from 'react';

const Projects = () => (
  <div className="content">
    <h3>Projects</h3>
    <ul>
      <li>
        <a href="http://snutt.kr">SNUTT</a>, Wafflestudio
        <ul>
          <li>
            Developed an web application of the course scheduling service using
            React & Redux.{' '}
            <a href="https://github.com/wafflestudio/snutt-webclient">
              GitHub Repo
            </a>{' '}
          </li>
        </ul>
      </li>
    </ul>
  </div>
);

export default Projects;
