import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  Education,
  Online,
  Projects,
  Teaching,
  WorkingExperience,
} from './index';
import Footer from '../footer';

storiesOf('Education', module).add('Render correctly', () => <Education />);
storiesOf('Online', module).add('Render correctly', () => <Online />);
storiesOf('Projects', module).add('Render correctly', () => <Projects />);
storiesOf('Teaching', module).add('Render correctly', () => <Teaching />);
storiesOf('WorkingExperience', module).add('Render correctly', () => (
  <WorkingExperience />
));
storiesOf('Footer', module).add('Render correctly', () => (
  <Footer currentTime={0} />
));

// export default {};
