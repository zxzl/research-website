import * as React from 'react';

const WorkingExperience = () => (
  <div className="content">
    <h3>Working Experience</h3>
    <ul>
      <li>
        Staff Backend Engineer, <a href="https://www.coupang.com/">Coupang</a>{' '}
        (2023 October ~ Current)
      </li>
      <li>
        Sr. Backend Engineer, <a href="https://www.coupang.com/">Coupang</a>{' '}
        (2021 May ~ 2023 October)
      </li>
      <ul>
        <li>At the ads relevance team, Coupang Media Group</li>
      </ul>
      <li>
        Software Engineer, <a href="https://www.navercorp.com/">Naver</a> (2019
        March ~ 2021 April)
      </li>
      <ul>
        <li>At the shopping search service team, Forest CIC</li>
      </ul>
      <li>
        Software Engineer Intern, <a href="http://sualab.com/">Sualab</a> (2015
        Summer)
        <ul>
          <li>
            Developed an edge detection algorithms for detecting badly packaged
            coin, using C++ and OpenCV
          </li>
          <li>
            Developed a dashboard for controlling label inspection system, using
            C# and WPF
          </li>
        </ul>
      </li>
    </ul>
  </div>
);

export default WorkingExperience;
