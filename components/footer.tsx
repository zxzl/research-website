import React from 'react';

interface Props {
  currentTime: number;
}

const Footer: React.SFC<Props> = ({ currentTime }) => {
  const date = new Date(currentTime);
  const dateString = date.toLocaleString('en-us', {
    month: 'long',
    year: 'numeric',
    day: 'numeric',
  });
  return (
    <footer className="footer">
      <div className="container">
        <div className="content has-text-centered">
          <p>
            <a href="https://gitlab.com/zxzl/research-website">This website</a>{' '}
            is built with <a href="https://github.com/zeit/next.js">Next.js</a>,{' '}
            <a href="https://bulma.io/">Bulma</a> and hosted at{' '}
            <a href="https://www.netlify.com/">Netlify</a>. <br />
            {`Last deployed at ${dateString}`}
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
