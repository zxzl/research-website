import * as React from 'react';
import * as T from '../../data/types';

export interface EntryProps {
  item: T.Publication;
}
const Entry: React.SFC<EntryProps> = ({
  item: { title, venue, authors, resources },
}) => (
  <div className="entry">
    <style jsx>
      {`
        .entry {
          margin-bottom: 0.7rem;
        }

        #me .name {
          text-decoration: underline;
        }

        .authors span:not(:last-child):after {
          content: ', ';
        }
      `}
    </style>
    <span className="has-text-weight-semibold">{`"${title}"`}</span>{' '}
    <span className="tag is-light">{venue}</span>
    <br />
    <span className="authors has-text-grey">
      {authors.map(author => {
        if ('name' in author) {
          return (
            <span key={author.name}>
              {author.link ? (
                <a href={author.link}>{author.name}</a>
              ) : (
                author.name
              )}
            </span>
          );
        }
        return (
          <span key="shik" id="me">
            <span className="name">Hyeungshik Jung</span>
          </span>
        );
      })}
    </span>
    <p>
      {resources &&
        Object.keys(resources).map(key => (
          <a href={resources[key]} key={key}>
            {`${key} `}
          </a>
        ))}
    </p>
  </div>
);

export default Entry;
