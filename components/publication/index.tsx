import * as React from 'react';
import Entry from './entry';

import * as T from '../../data/types';

interface PublicationsProps {
  works: T.Works;
}

const Publications: React.SFC<PublicationsProps> = ({ works }) => (
  <div className="content">
    <h3>Publications</h3>
    <span className="has-text-weight-semi-bold is-size-6">
      📄Conference Papers
    </span>
    <div>
      {works.conferences.map((entry, i) => (
        <Entry key={i} item={entry} />
      ))}
    </div>
    <span className="has-text-weight-semi-bold is-size-6">
      🏗️Posters & Workshop Papers
    </span>
    <div>
      {works.wips.map((entry, i) => (
        <Entry key={i} item={entry} />
      ))}
    </div>
  </div>
);

export default Publications;
