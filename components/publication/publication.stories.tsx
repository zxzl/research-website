import React from 'react';
import { storiesOf } from '@storybook/react';
import Entry from './entry';
import { works } from '../../data';
import Publications from './index';

const SamplePaper = works.conferences[0];

storiesOf('Publications', module)
  .add('Single Entry', () => <Entry item={SamplePaper} />)
  .add('Multiple Entry', () => <Publications works={works} />);

// export default {};
