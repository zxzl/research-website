import * as React from 'react';

const Intro: React.SFC<{}> = () => (
  <div className="content">
    <style jsx>
      {`
        #name-kor {
          font-size: 18px;
        }
        h1 {
          margin-bottom: 0.5em !important;
        }
      `}
    </style>
    <h1 className="title">
      Hyeungshik Jung
      <span id="name-kor">정형식</span>
    </h1>
    <p>
      I'm a Seoul-based{' '}
      <a
        href="https://github.com/kamranahmedse/developer-roadmap"
        target="_blank"
      >
        web developer
      </a>{' '}
      working at Coupang.
      <br /> I'm interested in building and shipping web services and let it
      evolve with more user.
      <br /> At grad school, I worked on video interfaces that leverage in-video
      objects as UI elements.
      {/* <br /> */}
      {/* <a href="/static/shik-cv.pdf">Curriculum Vitae</a> */}
    </p>
  </div>
);

export default Intro;
