import * as React from 'react';

import Header from '../components/header';
import Footer from '../components/footer';

interface Props {
  children: React.ReactNode;
  currentTime: number;
}

const Main: React.SFC<Props> = ({ children, currentTime }) => (
  <div>
    <Header />
    <section className="section">
      <div className="container">{children}</div>
    </section>
    <Footer currentTime={currentTime} />
  </div>
);

export default Main;
