import * as React from "react";

import Main from "../layouts/main";
import Intro from "../components/intro/index";
import Publications from "../components/publication";
import {
  WorkingExperience,
  Education,
  Projects,
  Online,
  Teaching
} from "../components/experiences";

import { works } from "../data";

interface Props {
  milliseconds: number;
}

class Index extends React.Component<Props> {
  static async getInitialProps() {
    const milliseconds = new Date().getTime();
    return { milliseconds };
  }

  render() {
    return (
      <Main currentTime={this.props.milliseconds}>
        <Intro />
        <WorkingExperience />
        <Projects />
        <Publications works={works} />
        <Teaching />
        <Education />
        <Online />
        <base target="_blank" />
      </Main>
    );
  }
}

export default Index;
