import * as T from './types';

const wips: T.Publication[] = [
  {
    title:
      'DynamicSlide: Exploring the Design Space of Reference-based Interaction Techniques for Slide-based Lecture Videos',
    venue:
      'MM 2018 (Workshop on Multimedia for Accessible Human Computer Interface)',
    resources: {
      pdf:
        'https://www.dropbox.com/s/39ut8xz2no974g3/mm2018-mahciworkshop-dynamicslide.pdf?dl=0',
      slide:
        'https://www.dropbox.com/s/vy6ro2lbse2qjrf/mm2018-mahciworkshop-dynamicslide-slides.pdf?dl=0',
    },
    authors: [
      {
        me: true,
      },
      {
        name: 'Hijung Valentina Shin',
        link: 'https://research.adobe.com/person/hijung-valentina-shin/',
      },
      {
        name: 'Juho Kim',
        link: 'http://juhokim.com',
      },
    ],
  },
  {
    title:
      'DynamicSlide: Reference-based Interaction Techniques for Slide-based Lecture Videos',
    venue: 'UIST 2018 (Poster)',
    resources: {
      pdf:
        'https://www.dropbox.com/s/c8hfb43sitvqw5q/uist2018-poster-dynamicslide.pdf?dl=0',
      poster:
        'https://www.dropbox.com/s/3tqc7c2vacsa8ne/uist2018-poster-dynamicslide-poster.pdf?dl=0',
    },
    authors: [
      {
        me: true,
      },
      {
        name: 'Hijung Valentina Shin',
        link: 'https://research.adobe.com/person/hijung-valentina-shin/',
      },
      {
        name: 'Juho Kim',
        link: 'http://juhokim.com',
      },
    ],
  },
];

export default wips;
