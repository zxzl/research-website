import Conferences from './conferences';
import WIPs from './wips';
import { Works } from './types';

const works: Works = {
  conferences: Conferences,
  wips: WIPs,
};

export { works };
