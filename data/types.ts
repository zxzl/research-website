export interface Collaborator {
  name: string;
  link?: string;
}

export interface Me {
  me: boolean;
}

export type Author = Collaborator | Me;

export interface Publication {
  title: string;
  venue: string;
  resources: Resources;
  authors: Array<Author>;
}

export interface Works {
  conferences: Publication[];
  wips: Publication[];
}

export interface Resources {
  [s: string]: string;
}

export const isMe = (author: Author): author is Me => {
  return !Boolean((<Collaborator>author).name);
};
