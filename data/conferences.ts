import * as T from './types';

const conferences: T.Publication[] = [
  {
    title:
      'RecipeScape: An Interactive Tool for Analyzing Cooking Instructions at Scale',
    venue: 'CHI 2018',
    resources: {
      website: 'http://recipescape.kixlab.org',
      video: 'https://www.youtube.com/watch?v=b1TccM1kN4U',
    },
    authors: [
      {
        name: 'Minsuk Chang',
        link: 'http://minsukchang.com',
      },
      {
        name: 'Leonore V. Guillain',
      },
      {
        me: true,
      },
      {
        name: 'Vivian M. Hare',
      },
      {
        name: 'Juho Kim',
        link: 'http://juhokim.com',
      },
      {
        name: 'Maneesh Agrawala',
        link: 'http://graphics.stanford.edu/~maneesh/',
      },
    ],
  },
];

export default conferences;
